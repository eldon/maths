#include <iostream>
#include <cstdlib>
#include <ctime>
#include <time.h>

using namespace std;

//this is for brute force

//this will return true if they find the subset numbers which eqaul the sum

bool SubsetSum (int Set[], int x, int sum) // dynamic programming
{

    //the value of subset[i] and [j] will be true  if subset of set[0..j-1] with sum equal to i
    bool Subset[sum+1][x+1];


    //if sum is found to be 0 then the answer will be true

    for (int i = 0; i <= x; i++)
        Subset[0][i]= true; //this is the operation to check the following

    //now we are going to make checks for times when the sum is not 0 and set is
    //the answer should resort to Zero

    for(int i = 1; i <=sum; i++)
         Subset[i][0]= false;

    //now we are going to fill the subset table in botton then up manner
    // two for loops are need for this

    for(int i=1 ; i<=sum; i++)
    {
        for(int j = 1; j <=x;j++)
        {
            Subset[i][j] = Subset [i][j-1];
            if (i>=  Set[j-1])
                Subset[i][j] = Subset[i][j] ||

                Subset[i - Set[j-1]][j-1];

                        }
    }

     return Subset[sum][x];
}

//creating the program for Greedy Algorithms
//first we have to print the number of maximum set activities that can be done by one computation at a time

//x is the number of activities
// Set []
//void Greedy (int Set [], int )

//now we are moving on to the test of the program to make sure that its all working

int main()


{
 clock_t start = clock();
for (int z = 99; z >= 0; --z)
{
std::srand(std::time(0));//using the current time as a seed for the randum no generator
int Size = 10; // amount of numbers we want generated
 int Set[Size] ; // the number of subset items which we have
int sum = 5 ; // the target which we are looking for
          //generating random numbers
    for (int i=0;i<Size; i++ )
    {
        Set[i]= (rand()%10)+1; //how large is the maximum number
       cout<< Set[i]<<endl; // generating random numbers which we will need for out set
    }
int x = sizeof(Set)/sizeof(Set[0]);
if (SubsetSum(Set,x,sum)==true) // calling the function which we created above
   cout<<"\n\nfound that subset with given sum"<<"\n"<<sum; // when the sum has been found
  else
    cout<<"\n\nNo subset with that given sum"; // when the sum hasnot been found


 clock_t stop = clock();
 double elapsed =(double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;
 cout<< "\n\nTime of: "<< elapsed;

    }
  return 0;
}
