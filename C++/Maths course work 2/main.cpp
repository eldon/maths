#define u64 unsigned long long  //or: uint64_t

#define MAX_SET_SIZE   100  // Maximum size of the set S
#define MAX_BIT_LENGTH  64  // Maximum bit length of the integers in S

#include <iostream>
#include <iomanip>          // for: setw()
#include <ctime>            // for: time()
#include <algorithm>        // for: sort()
//#include "rand/mt64.h"      // for: 64 bit pseudo-random numbers generation
#include <cmath>
using namespace std;

/*start off by first creating some classes for the set */


u64 random64();             // random 64 bit number
u64 random64(u64 a, u64 b); // random numbers between a and b
u64 gcd(u64 a, u64 b);      // Greatest Common Divisor of a and b

class Set
{
public:
    int u64 S[MAX_SET_SIZE];
    size_t  n ;         // the set size
    u64     t ;         // the target
    u64     maxSubs ;   // this the maximum number of subsets 2^n
//-------------------------------------------------------------
    void reset()
    {
        n= t= S[0] = 0; maxSubs = 1;
    }
//--------------------------------------------------------------
    Set()
    {
        reset();
    }
//-------------------------------------------------------------
    Set(int m, bool rnd = 0)
    {
        fillIn(m, rnd);
    }
//--------------------------------------------------------------
    bool add(u64 elem)
    {
        if(n == MAX_SET_SIZE-1) return 0;
        for (size_t i = 0 ; i < n ; i++)
            if( S[i]== elem) return 0;
        S[n]= elem;
        n   = n + 1;
        maxSubs  = 2*maxSubs ; //2^n subset 0...2^n-1 or Φ ... A
        return 1;
    }
//--------------------------------------------------------------
    u64& operator[](size_t i)
    {
        if (i > n-1 ) i = n-1 ;
        return S[i];
    }
//--------------------------------------------------------------
    void print()
    {
        cout<<"Set = {"; for(size_t i=0 ; i < n ; i++) cout<<S[i]<<" "; cout<<"} and has "<<n<<" elements "<<endl;
         //for(size_t i=0 ; i < n ; i++) cout<<"___";
        //cout<<endl<<endl;//filling in finished
     }
//--------------------------------------------------------------
    bool trim(size_t i_To_n)  // trim form i onwards
    {
        if (i_To_n > n) {cout<<"trim cannot be performed"<<endl; return 0;}
        n   = i_To_n;
        maxSubs  = pow(2,n);
        return 1;
    }
//---------------------------------------------------------------
    bool trim() /* this must not be called for partition problem*/
    {
       for(size_t i =0 ; i < n ; i++)
        if (S[i]> t && trim(i)){
            cout<<"Target = "<<t<< " set has been trimmed from "<<S[i]<<" onwards ";
            print();
            return 1;
        }
    return 0;
    }
//--------------------------------------------------------------
    void Sort() // this is insertion sort
    {
        size_t i, j
        u64    key;
        for(j = 1; j < n; j++){
            key = S[j];
            for(i = j - 1; (i >= 0) && (S[i] > key); i--) S[i+1] = S[i];
            S[i+1] = key;
         }
     return;
     }
//--------------------------------------------------------------
    u64 generateRand(int bitlength = 5*8)
    {
       srand(clock()); // Use current time as seed for the random numbers generator
       init_genrand64(clock());
       u64 MASK = (1ULL<<bitlength)-1ULL; // ULL suffix makes any number as Unsigned Long Long
       u64 r= ((double) random64()/RAND_MAX) + 1;
       return (r & MASK);
    }
//--------------------------------------------------------------
    void fillIn(size_t m, int bitlength = 4*8 , bool rnd = 1, bool prnt=0) /* bitlength= 5x8bytes numbers...****Note that bitlength should be varied for Dynamic Programming*/
    {
        reset();
        for(u64 i=0 ; i < m ; i++){
            u64 r;
            do{
                r = i + 1;
                if(rnd) r = generateRand(bitlength);
            }
            while(!add(r));
        }
        Sort();
        if(prnt) print();
        //Sleep(100); // could be deleted this is to give some time delay, use sleep(100) instead for Mac and Linux users
     }
//--------------------------------------------------------------
    u64 Sum()
    {
        u64 sum = 0;
        for (size_t i = 0 ; i < n ; i++)
            sum += S[i];
        return sum;
    }

};
//----------------------------------------------------------------------------------
/**This class is what made the code very efficient!
you only need to understand how to use it not how it was built!*/

class SubSet
{
public:
    static Set A;// this the set that we are going to obtain its subsets
    u64 id;    // this the is of the subset Sub[0] = φ phi= empty set
//--------------------------------------------------------------
    void fillA(size_t m, int bitlength = 4*8 , bool rnd = 1, bool prnt=0)
    {
        A.fillIn(m,bitlength,rnd,prnt);
    }
//--------------------------------------------------------------
    SubSet(u64 sub_id=0)
    {
        id = sub_id;
        if (id > A.maxSubs-1)  id = A.maxSubs-1;
    }
//--------------------------------------------------------------
    SubSet operator()(u64 Newid)
    {
        id = Newid ;
        if (id > A.maxSubs-1)  id = A.maxSubs-1;
        return *this;
    }
//--------------------------------------------------------------
    u64 Sum()
    {
        u64 sum = 0;
        for (size_t i = 0 ; i < A.n ; i++)
            if(id & ( 1 << i )) sum += A[i];
        return sum;
    }
//--------------------------------------------------------------
    void print(bool endl_ = false)
    {
        cout<<"Subset("<<id<<") { ";
        for (size_t i = 0 ; i < A.n ; i++)
            if(id & ( 1 << i )) cout<<A[i]<<" ";
        cout<<"}" ;
        if(endl_) cout<<endl;
    }
//--------------------------------------------------------------
    void print_Sum()
    {
        print();
        cout<<" = " <<Sum() <<endl;
    }
//--------------------------------------------------------------
    void print_indicator()
    {
        static int i = 5;
        if(rand() < (RAND_MAX/10000)){string str =  "..... \r"; str[i%5] = ' '; cout<<str;}
        i--;
        if(!i) i = 5;
    }
//--------------------------------------------------------------
    SubSet& operator+(u64 elem)  // adding an element from A to the subset
    {
        size_t i;
        for(i =0; i< A.n ; i++) if (A[i]== elem) break;
        if(i== A.n) {cout<<"element cannot be added"<<endl;return *this;}
        id = id |( 1 << i );
        return *this;
    }
//--------------------------------------------------------------
    friend SubSet operator+(SubSet lhs,const SubSet& rhs) // subset Unification
    {
        lhs.id = lhs.id | rhs.id;
        return lhs;
    }
//--------------------------------------------------------------
    friend SubSet operator-(SubSet lhs,const SubSet& rhs) // subset difference
    {
        lhs.id = lhs.id & (~rhs.id);
        return lhs;
    }
//--------------------------------------------------------------
    SubSet& operator!() /* subset Complement you might need to use this in the 2-partition problem*/
    {
     SubSet Univ(A.maxSubs-1);
     *this = Univ - *this ;
     return *this;
    }
//--------------------------------------------------------------
    bool isE()  {if(id == 0)              return 1; return 0; } /* is the subset empty, this is suitable for * intersection */
    bool isA()  {if(id == (A.maxSubs-1))  return 1; return 0; } /* is the subset == A , this is suitable for + unification  */
//--------------------------------------------------------------
    friend SubSet operator*(SubSet lhs,const SubSet& rhs) // subset intersection
    {
        lhs.id = lhs.id & rhs.id;
        return lhs;
    }
//---------------------------------------------------------------
    friend bool operator==(const SubSet& lhs, const SubSet& rhs) // comparing two subsets
    {
        return (lhs.id == rhs.id) ;
    }
//---------------------------------------------------------------
    friend bool operator!=(const SubSet& lhs, const SubSet& rhs) // comparing two subsets
    {
        return (lhs.id != rhs.id) ;
    }
};
Set SubSet::A; // this is essential for static class members to be used and for us to refer to A directly without SubSet::A.
Set &A = SubSet::A;
//===============================================================
class Algorithms{
public:
    SubSet Sub;
    u64 T; //this is going to be varying target for 2partition
//----------------------------------------------------------------
    bool PartitionExist(bool prnt=0)
    {
        u64 SumA = A.Sum();
        if(SumA==0){
            if(prnt){ cout<<"solution is trivial A is empty"<<endl<<endl; A.print();}
            return 0;
        }
        if(SumA%2 ){
            if(prnt){cout<<"Impossible to find a 2-partition, Sum of A = "<< SumA<<" is odd "<<endl<<endl;}
            return 0;
        }
    return 1;
    }
//----------------------------------------------------------------
    SubSet BruteForce() /**this code has been given to you as complete to act as an example of how to use the provided classes*/
    {
        u64 target = A.Sum()/2; // do not worry if sum is odd; PartitionExist take care of that
        for(u64 i = 1; i< A.maxSubs ; i++){
            Sub(i).print_indicator(); // this is unnecessary just for showing that it is calculating
            if((Sub(i).Sum() == target)){
                Sub(i).print_Sum();   (!Sub(i)).print_Sum();
                /**  nothing is missing here!!!
                cout<<"-----------------------------"<<endl;
                cout<<"1: Unification = ";(Sub(i) + !Sub(i)).print_Sum();
                cout<<"2: Intersection= ";(Sub(i) * !Sub(i)).print_Sum();
                */
                cout<<endl<<endl;
                return Sub(i);
            }
        }
        cout<<"target "<< target <<" not found...."<<endl<<endl;
        return false;
    }
//-----------------------------------------------------------------
    bool Dynamic() /**this code has been tested and some lines has bee removed complete as appropriate*/
    {

        const u64 target = A.Sum()/2;// do not worry if sum is odd; PartitionExist take care of that
        bool **SubTable = new bool*[A.n + 1];// avoids crashing up to an extent (bitlength<than3 bytes)
        /**we need a dynamic arrays otherwise if the target is big then we will consume the stack
        //and the program will crash. In dynamic we run into space complexity issues*/
        for(size_t r = 0; r <= A.n; ++r)
            SubTable[r] = new bool[target +1];

        for(u64 c = 1 ; c <= target ; c++) SubTable[0][c]=0;
        for(size_t r = 0 ; r <= A.n ; r++) SubTable[r][0]=1;

        for(size_t r = 1 ; r <= A.n   ; r++){
            for(u64 c = 1 ; c <= target ; c++){
                Sub.print_indicator(); // this is unnecessary just for showing that it is calculating
              /**missing code goes here................few lines are needed have a look at the lecture slides*/
                SubTable[r][c] = SubTable[r-1][c];
                if(c >= A[r-1]){
                     SubTable[r][c] = SubTable[r][c] || SubTable[r-1][c - A[r-1]];
                }
            }
        }
        if(SubTable[A.n ][target])  cout<< "solution exists"<<endl;
        else                        cout<< "no solution!!!!"<<endl;

/** the following code is to display the Dynamic Table, useful for debugging small values of n*/
/**
        cout<<"         ";
        for(size_t c = 0   ; c <= target ; c++) cout<<c<<" ";cout<<endl;
        cout<<"        ------------"<<endl;
        for(size_t r = A.n ; r > 0       ; r--){
            cout<<"A["<<r<<"]="<<A[r-1]<<" | ";
            for(size_t c = 0 ; c <= target ; c++){
                cout<<SubTable[r][c] <<" ";
            }
            cout<<" \n";
        }
*/
        return SubTable[A.n ][target];
    }
//----------------------------------------------------------------
    SubSet Greedy()
    {
        u64 target = A.Sum()/2; // do not worry if sum is odd; PartitionExist take care of that
        A.Sort(); // not really necessary as Sort() is called when A is filled
        Sub(0); //start with an empty set
        cout<<"target = "<<target<<endl;
        for(int i= A.n-1 ; i >=0 ; i--)  /** careful do not do -target this will give you ridiculous numbers because it is unsigned long long int and the sign to it means the complement of its complete range*/
//        if(/** place a suitable condition here!*/)
        if(target > Sub.Sum()+ A[i])
                Sub = Sub + A[i];

    Sub.print_Sum(); (!Sub).print_Sum();
    return Sub;
    }
//----------------------------------------------------------------
    SubSet SimAnnealing()
    {
        u64 target = A.Sum()/2; // do not worry if sum is odd; PartitionExist take care of that
        A.Sort(); // not really necessary as Sort() is called when A is filled
        Sub(0);
    /** your code for the Simulated annealing goes here*/

    return Sub;
    }

//-----------------------------------------------------------------
    void Testing( size_t Algorithm ,int bitlength = 3*8 , bool rnd = true)// no need to change this function
    {
        srand(clock()*time(0));

        const size_t maxn = 40;  /** #maxn = 40  for the assignment*/
        const size_t step = 10;   /** #step = 10 for the assignment*/
        double times[maxn/step];

        for(size_t n = step ; n <= maxn ; n+=step){
            clock_t startT = clock();
            size_t Tests = 10;/**#Tests should be 10 for the assignment*/
            for(size_t j = 1; j<=Tests ; j++){
                do{
                    A.fillIn(n,bitlength, rnd);
                } while(!PartitionExist() && rnd);
                A.print();
                if(!rnd && !PartitionExist(!rnd) ) continue;
                if(Algorithm == 1)  BruteForce();
                if(Algorithm == 2)  Dynamic();
                if(Algorithm == 3)  Greedy();
                if(Algorithm == 4)  SimAnnealing();
            }
            times[n/step] =  (clock()-startT) /Tests;// /CLOCKS_PER_SEC ;
        }
        for(size_t n = step ; n <= maxn ; n+=step)cout<<"n = "<< n <<" time = "<<(double)times[n/step]<<endl; // display the times
    }
//---------------------------------------------------------------
};
//===============================================================
int main()
{
/** Do not forget to unzip the folder for the project in order to compile properly*/
/**You do not need to change the code here unless if you want to test specific algorithm then change the Question in the for loop
Go to the provided functions
    Q1: BruteForce()
    Q2: Dynamic()
    Q3: Greedy()
    Q4: SimAnnealing()
and add or change there

The bitlength
    For the BruteForce() should be fixed to no more than 4*8 or higher up to 7*8 otherwise it will take very long time
    For Dynamic() it should be varied between 1*8...2.5*8 or 3*8 in order to see the effect of it, more than that it could crash the program due to RM space issues.
    For Greedy() and SimAnnealing()  it can be given higher values as it is approximate

    The below code is ready for you to test your algorithms and approaches,
    if you would like to test with fewer elements go to Test and change maxn or step
*/
    Algorithms Alg;
    for(int Question = 1 ; Question <= 4; Question++){
        bool rnd = true ;// for testing the algorithms this should be true;
        int  bitlength = 8*2.5;//#of bytes this should be varied especially for Dynamic

        Alg.Testing(Question,bitlength, rnd);
        cout<<" Algorithm "<<Question << " finished!............................................................"<<endl<<endl<<endl;
    }
    return 0;
}
