def search(values, max_iter, max_no_improv, alpha)
  rasult = nil
  max_iter.times do |iter|
    candidate = construct_randomized_greedy_solution(values, alpha);
    candidate = local_search(candidate, values, max_no_improv)
    rasult = candidate if rasult.nil? or candidate[:cost] < rasult[:cost]
    puts " > iteration #{(iter+1)}, rasult=#{rasult[:cost]}"
  end
  return rasult
end

def random(permutation)
  perm = Array.new(permutation)
  c1, c2 = rand(perm.size), rand(perm.size)
  exclude = [c1]
  exclude << ((c1==0) ? perm.size-1 : c1-1)
  exclude << ((c1==perm.size-1) ? 0 : c1+1)
  c2 = rand(perm.size) while exclude.include?(c2)
  c1, c2 = c2, c1 if c2 < c1
  perm[c1...c2] = perm[c1...c2].reverse
  return perm
end

def length(c1, c2)
  Math.sqrt((c1[0] - c2[0])**2.0 + (c1[1] - c2[1])**2.0).round
end

def local_search(rasult, values, max_no_improv)
  count = 0
  begin
    candidate = {:vector=>random(rasult[:vector])}
    candidate[:cost] = cost(candidate[:vector], values)    
    count = (candidate[:cost] < rasult[:cost]) ? 0 : count+1
    rasult = candidate if candidate[:cost] < rasult[:cost]    
  end until count >= max_no_improv
  return rasult
end

def construct_randomized_greedy_solution(values, alpha)
  candidate = {}
  candidate[:vector] = [rand(values.size)]
  allvalues = Array.new(values.size) {|i| i}
  while candidate[:vector].size < values.size
    candidates = allvalues - candidate[:vector]
    costs = Array.new(candidates.size) do |i| 
      length(values[candidate[:vector].last], values[i])
    end
    rcl, max, min = [], costs.max, costs.min
    costs.each_with_index do |c,i| 
      rcl << candidates[i] if c <= (min + alpha*(max-min))
    end
    candidate[:vector] << rcl[rand(rcl.size)]
  end
  candidate[:cost] = cost(candidate[:vector], values)
  return candidate
end

def cost(perm, values)
  distance =0
  perm.each_with_index do |c1, i|
    c2 = (i==perm.size-1) ? perm[0] : perm[i+1]
    distance += length(values[c1], values[c2])
  end
  return distance
end

begining_time = Time.now 
berlin52 = [[9,9],[8,7],[8,4],[6,2],[1,5],#,]
 [1,2],[3,4],[5,6],[7,8],[9,10],
 [0,1],[2,3],[4,7],[8,5],[2,6],
 [4,5],[2,1],[4,7],[6,5],[8,4],
 [4,2],[5,2],[8,9],[1,5],[5,3],
 [3,8],[6,4],[6,2],[2,6],[2,3],
 [9,1],[7,5],[3,5],[7,6],[1,5]]
length = 50
max_iter = length
max_no_improv = length
greediness_factor = 0.3
rasult = search(berlin52, max_iter, max_no_improv, greediness_factor)
end_time = Time.now
puts "Done. rasult Solution: c=#{rasult[:cost]}, v=#{rasult[:vector].inspect} done in #{end_time - begining_time}"
