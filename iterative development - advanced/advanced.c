#include <stdio.h>

int Search(int array[], int l, int s, int a) // to returns the location of search in the array
{
   if (s >= l)
   {
        int mid = l + (s - l)/2;

        if (array[mid] == a)  return mid; // if the value exists in the mid-point

        if (array[mid] > a) return Search(array, l, mid-1, a); // if value is smaller the mid-point value, then it is present in left side of the array

        return Search(array, mid+1, s, a); // else the value is present on right side of array
   }

   return -1; //return this when value does not exist in the array
}

int main(void)
{
   int array[] = {2, 3, 4, 10, 40}; // values that are stored in the array
   int i = sizeof(array)/ sizeof(array[0]); // splits array to determine which side to search on
   int a = 10; // the value we are searching for
   int answer = Search(array, 0, i-1, a);
   (answer == -1)? printf("Element is not present in array") // returns when value is not found
                 : printf("Element is present at index %d", answer); //returns location point of the value
   return 0;
}
